package com.mars.sql.manager.api;

import com.mars.cloud.core.annotations.NotRest;
import com.mars.core.annotation.MarsApi;
import com.mars.core.annotation.MarsReference;
import com.mars.sql.dto.UserInfoDTO;

/**
 * 维护人员登录
 */
@NotRest
@MarsApi
public interface LoginApi {

    /**
     * 登录接口
     * @param userInfoDTO
     * @return
     */
    @MarsReference(beanName = "",refName = "")
    Object login(UserInfoDTO userInfoDTO);
}
