package com.mars.sql.manager.inters;

import com.mars.core.annotation.MarsInterceptor;
import com.mars.mvc.base.BaseInterceptor;
import com.mars.server.server.request.HttpMarsRequest;
import com.mars.server.server.request.HttpMarsResponse;

/**
 * 登录拦截器
 */
@MarsInterceptor
public class LoginInters implements BaseInterceptor {

    public Object startRequest(HttpMarsRequest request, HttpMarsResponse response) {
        return SUCCESS;
    }

    public Object endRequest(HttpMarsRequest request, HttpMarsResponse response, Object obj) {
        return SUCCESS;
    }
}
