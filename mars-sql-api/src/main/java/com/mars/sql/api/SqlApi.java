package com.mars.sql.api;

import com.mars.core.annotation.MarsApi;
import com.mars.core.annotation.MarsReference;
import com.mars.core.annotation.RequestMethod;
import com.mars.core.annotation.enums.ReqMethod;
import com.mars.sql.dto.SqlInfoDTO;

/**
 * 外部获取sql语句的接口
 */
@MarsApi
public interface SqlApi {

    /**
     * 获取sql信息对象
     * @param sqlName
     * @return
     */
    @RequestMethod(ReqMethod.POST)
    @MarsReference(beanName = "sqlExecuteService",refName = "getSqlInfoDTO")
    SqlInfoDTO getSqlInfoDTO(String sqlName);
}
