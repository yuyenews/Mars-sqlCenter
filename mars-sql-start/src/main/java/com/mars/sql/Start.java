package com.mars.sql;

import com.mars.start.StartMars;

/**
 * 启动项目
 */
public class Start {

    public static void main(String[] args) {
        StartMars.start(Start.class);
    }
}
