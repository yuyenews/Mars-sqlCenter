package com.mars.sql.dto;

import java.util.Date;

/**
 * sqlInfo表的实体类
 */
public class SqlInfoDTO {

    private Integer id;//'主键',
    private String sql_name;//'Sql名称，唯一，只能为英文字母和数字',
    private String sql_remark;//'对这条sql的说明备注',
    private String module_code;//'模块code',
    private String sql_content;//'sql语句',
    private String data_source;//'数据源名称',
    private Integer status;//'状态 0 可用 1停用',
    private Date create_time;//'创建时间',
    private String create_by;//创建人code
    private String moduleName;//模块名称

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSql_name() {
        return sql_name;
    }

    public void setSql_name(String sql_name) {
        this.sql_name = sql_name;
    }

    public String getSql_remark() {
        return sql_remark;
    }

    public void setSql_remark(String sql_remark) {
        this.sql_remark = sql_remark;
    }

    public String getModule_code() {
        return module_code;
    }

    public void setModule_code(String module_code) {
        this.module_code = module_code;
    }

    public String getSql_content() {
        return sql_content;
    }

    public void setSql_content(String sql_content) {
        this.sql_content = sql_content;
    }

    public String getData_source() {
        return data_source;
    }

    public void setData_source(String data_source) {
        this.data_source = data_source;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
}
