package com.mars.sql.manager.dao;

import com.mars.core.annotation.MarsDao;

/**
 * 用户信息dao
 */
@MarsDao("userInfoDAO")
public abstract class UserInfoDAO {
}
