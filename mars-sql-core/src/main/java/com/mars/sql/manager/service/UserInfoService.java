package com.mars.sql.manager.service;

import com.mars.core.annotation.MarsBean;
import com.mars.core.annotation.MarsWrite;
import com.mars.sql.manager.dao.UserInfoDAO;

/**
 * 用户信息服务层
 */
@MarsBean("userInfoService")
public class UserInfoService {

    @MarsWrite("userInfoDAO")
    private UserInfoDAO userInfoDAO;


}
