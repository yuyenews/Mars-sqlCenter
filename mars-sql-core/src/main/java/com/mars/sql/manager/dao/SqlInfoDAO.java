package com.mars.sql.manager.dao;

import com.mars.core.annotation.DataSource;
import com.mars.core.annotation.MarsDao;
import com.mars.mj.annotation.MarsSelect;
import com.mars.mj.annotation.MarsUpdate;
import com.mars.mj.enums.OperType;
import com.mars.mj.helper.model.PageModel;
import com.mars.mj.helper.model.PageParamModel;
import com.mars.mj.helper.templete.JdbcTemplate;
import com.mars.sql.dto.SqlInfoDTO;

import java.util.Map;

/**
 * sqlDAO
 */
@MarsDao("sqlInfoDAO")
public abstract class SqlInfoDAO {


    /**
     * 根据sqlName查询sql
     * @param sqlInfoDTO
     * @return
     */
    @MarsSelect(sql = "select id,sql_name,sql_remark,module_code,sql_content,data_source,`status`,create_time,create_by from sql_info where sql_name = #{sql_name}",resultType = SqlInfoDTO.class)
    @DataSource("sqlCenter")
    public abstract SqlInfoDTO selectSqlInfoDTO(SqlInfoDTO sqlInfoDTO);

    /**
     * 根据主键修改sql信息
     * @param sqlInfoDTO
     * @return
     */
    @MarsUpdate(tableName = "sql_info",primaryKey = "id",operType = OperType.UPDATE)
    @DataSource("sqlCenter")
    public abstract SqlInfoDTO updateSqlInfoDTO(SqlInfoDTO sqlInfoDTO);

    /**
     * 新增sql信息
     * @param sqlInfoDTO
     * @return
     */
    @MarsUpdate(tableName = "sql_info",primaryKey = "id",operType = OperType.INSERT)
    @DataSource("sqlCenter")
    public abstract SqlInfoDTO infoDTOSqlInfoDTO(SqlInfoDTO sqlInfoDTO);

    /**
     * 分页查询列表
     * @param pageParamModel
     * @return
     * @throws Exception
     */
    public PageModel<SqlInfoDTO> selectSqlInfoPage(PageParamModel pageParamModel) throws Exception {
        StringBuffer sql = new StringBuffer();
        return JdbcTemplate.get().selectPageList(sql.toString(),pageParamModel,SqlInfoDTO.class);
    }
}
