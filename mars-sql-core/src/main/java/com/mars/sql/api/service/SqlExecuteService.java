package com.mars.sql.api.service;

import com.mars.core.annotation.MarsBean;
import com.mars.core.annotation.MarsWrite;
import com.mars.sql.dto.SqlInfoDTO;
import com.mars.sql.manager.dao.SqlInfoDAO;

/**
 * 执行查询出来的sql
 */
@MarsBean("sqlExecuteService")
public class SqlExecuteService {

    @MarsWrite("sqlInfoDAO")
    private SqlInfoDAO sqlInfoDAO;

    /**
     * 获取sqlInfo对象
     * @param sqlName
     * @return
     */
    public SqlInfoDTO getSqlInfoDTO(String sqlName){
        SqlInfoDTO sqlInfoDTO = new SqlInfoDTO();
        sqlInfoDTO.setSql_name(sqlName);
        return sqlInfoDAO.selectSqlInfoDTO(sqlInfoDTO);
    }
}
