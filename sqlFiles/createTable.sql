SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for module_info
-- ----------------------------
DROP TABLE IF EXISTS `module_info`;
CREATE TABLE `module_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `module_code` varchar(255) DEFAULT NULL COMMENT '模块code，唯一 只能是数字和字母',
  `module_name` varchar(255) DEFAULT NULL COMMENT '模块名称',
  `remark` varchar(1000) DEFAULT NULL COMMENT '模块说明备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sql_info
-- ----------------------------
DROP TABLE IF EXISTS `sql_info`;
CREATE TABLE `sql_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sql_name` varchar(255) DEFAULT NULL COMMENT 'Sql名称，唯一，只能为英文字母和数字',
  `sql_remark` varchar(1000) DEFAULT NULL COMMENT '对这条sql的说明备注',
  `module_code` varchar(255) DEFAULT NULL COMMENT '模块code',
  `sql_content` varchar(2000) DEFAULT NULL COMMENT 'sql语句',
  `data_source` varchar(255) DEFAULT NULL COMMENT '数据源名称',
  `status` int(11) DEFAULT NULL COMMENT '状态 0 可用 1停用',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(0) DEFAULT NULL COMMENT '创建人username',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名 登录用的，只可以为字母和数字，不可重复',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `role` int(11) DEFAULT NULL COMMENT '角色： 0 所有权限 1 只读权限',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Sql中心用户表';

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES (1, 'admin', '123456', '管理员', '2019-10-30 21:08:16', 0, '超级管理员');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
